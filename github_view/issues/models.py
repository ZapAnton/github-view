from django.db import models


class Issue(models.Model):
    title = models.CharField(max_length=1000)

    open_date = models.DateTimeField('Date when an issue was opened')
