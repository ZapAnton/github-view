from django.urls import path


from issues import views

urlpatterns = [
    path('', views.index, name='index'),
]
